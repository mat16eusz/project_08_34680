# PIZZA AdMat
The pizzeria menu and basket system was created for the needs of the university project.


## Table of Contents
* [Technologies Used](#technologies-used)
* [Usage](#usage)
* [Distribution of tasks](#distribution-of-tasks)
* [Created by](#created-by)


## Technologies Used
* C++ 14
* Standard Template Library (STL)


## Usage
1. Clone the repository by running the command below on your git terminal.
```
git clone https://Mat16eusz@bitbucket.org/mat16eusz/project_08_34680.git
```
2. Open the project in VisualStudio and run the compilation.
44

## Distribution of tasks
| Mateusz Jasiak                             | Adrian Ciocho�                             |
|--------------------------------------------|--------------------------------------------|
| function read write to file                | function of selecting beverages and pizzas |
| creating documentation                     | creating a receipt                         |


## Created by:
* Mateusz Jasiak    [BITBUCKET](https://bitbucket.org/mat16eusz)
* Adrian Ciocho�    [BITBUCKET](https://bitbucket.org/adrianciochon)