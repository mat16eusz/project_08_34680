/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#ifndef READ_FILE_H_
#define READ_FILE_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * The function reads the given file and saves the data in the program.
 *
 * @brief Reading the file.
 * @param string file_name: filename to read the file.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int read_file(string file_name);

/**
 * A function that reads a file to find a specific product and writes it to the program.
 *
 * @brief Reading one product from a file.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param int indeks: index number to read the product.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int read_file_one_product(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, int indeks, vector<int>& type_product_tab);

#endif
