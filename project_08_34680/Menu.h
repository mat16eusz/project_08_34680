/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#ifndef MENU_H_
#define MENU_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * A function that displays a menu that allows you to order products and make payments.
 *
 * @brief Displays menu.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void menu(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab);

#endif
