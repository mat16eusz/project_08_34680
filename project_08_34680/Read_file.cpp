/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#include "Read_file.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>

using namespace std;

/**
 * The function reads the given file and saves the data in the program.
 *
 * @brief Reading the file.
 * @param string file_name: filename to read the file.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int read_file(string file_name)
{
	fstream file;
    unsigned number_product;
    string name_product;
    vector<string> product_tab;

    file.open(file_name, ios::in);

    if (file.good() != true)
    {
        cout << "Nie mo�na otworzy� pliku" << endl;
        file.close();

        return 1;
    }

    file >> number_product;

    file.ignore(1);
    for (int i = 0; i < number_product; i++)
    {
        getline(file, name_product);
        product_tab.push_back(name_product);
    }

    file.close();

    for (int i = 0; i < number_product; i++)
    {
        cout << product_tab[i] << endl;
    }

    return 0;
}

/**
 * A function that reads a file to find a specific product and writes it to the program.
 *
 * @brief Reading one product from a file.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param int indeks: index number to read the product.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int read_file_one_product(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, int indeks, vector<int>& type_product_tab)
{
    fstream file;
    unsigned number_product;
    string name_product;
    vector<string> product_tab;
    int pos_name, pos_price;
    float price;

    file.open(file_name, ios::in);

    if (file.good() != true)
    {
        cout << "Nie mo�na otworzy� pliku" << endl;
        file.close();

        return 1;
    }

    file >> number_product;

    file.ignore(1);
    for (int i = 0; i < number_product; i++)
    {
        getline(file, name_product);
        product_tab.push_back(name_product);
    }

    file.close();

    pos_name = product_tab[indeks].find(" ");
    name_product_tab.push_back(product_tab[indeks].substr(2, pos_name - 2));

    pos_price = product_tab[indeks].find("z");
    price = stof(product_tab[indeks].substr(pos_name + 3, pos_price - pos_name - 3));
    price_product_tab.push_back(price);

    if (file_name == "Offer/Pizza.txt")
    {
        type_product_tab.push_back(1);
    }
    else
    {
        type_product_tab.push_back(0);
    }

    return 0;
}
