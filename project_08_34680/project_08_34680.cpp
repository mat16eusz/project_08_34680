﻿/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciochoń
 * @author    34275@student.pwsztar.edu.pl
 */

#include <iostream>
#include <vector>
#include <windows.h>
#include "Menu.h"

using namespace std;

int main()
{
    vector<string> name_product_tab;
    vector<float> price_product_tab;
    vector<int> type_product_tab;

    SetConsoleOutputCP(1250);

    menu(name_product_tab, price_product_tab, type_product_tab);

    return 0;
}
