/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#include "Pizza.h"
#include <iostream>
#include <string>
#include "Read_file.h"
#include "Menu.h"

using namespace std;

/**
 * A function that displays available promotions in a pizzeria.
 *
 * @brief Displays promotion in pizzeria.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void promotion(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab)
{
    string select;
    int select_int;

    read_file("Offer/Promotions.txt");
    cout << "1. Wr��" << endl;
    cout << "Wyb�r opcji: ";

    while (select != "1")
    {
        cin >> select;
        while (getchar() != '\n');

        if (select != "1")
        {
            printf("Nieprawid�owy wyb�r!\n");
            printf("Wybierz ponownie opcj�: ");
        }
    }
    select_int = stoi(select);

    switch (select_int)
    {
    case 1:
        system("cls");
        menu(name_product_tab, price_product_tab, type_product_tab);
        break;

    default:
        break;
    }
}
