/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#ifndef RECEIPT_H_
#define RECEIPT_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * The function saves the receipt for orders based on the basket.
 *
 * @brief Saving a receipt.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param string promotion_fifty_fifty: activation of the fifty fifty promotion.
 * @param float price: order price.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int write_file(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, string promotion_fifty_fifty, float price);

/**
 * Function that generates a receipt for orders based on the basket.
 *
 * @brief Generating a receipt.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param float price: order price.
 */
void receipt_generate(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, float price);

#endif
