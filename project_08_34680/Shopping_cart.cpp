/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#include "Shopping_cart.h"
#include <iostream>
#include <vector>
#include <iomanip> 
#include "Read_file.h"
#include "Pizza.h"
#include "Drink.h"
#include "Menu.h"
#include "Receipt.h"

using namespace std;

/**
 * The function adds the selected product to the basket.
 *
 * @brief Adding a product to the cart.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param int indeks: index number to read the product.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void add_product_to_shopping_cart(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, int indeks, vector<int>& type_product_tab)
{
	read_file_one_product(file_name, name_product_tab, price_product_tab, indeks, type_product_tab);
	if (file_name == "Offer/Pizza.txt")
	{
		pizza(name_product_tab, price_product_tab, type_product_tab);
	}
	else
	{
		drink(name_product_tab, price_product_tab, type_product_tab);
	}
}

/**
 * Function responsible for imposing the fifty fifty promotion.
 *
 * @brief Fifty Fifty Promotion Calculation.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return int: index of the second pizza.
 */
int calculate_promtion_fifty_fifty(vector<float>& price_product_tab, vector<int>& type_product_tab)
{
	int temp_index = 0;

	for (int i = 0; i < type_product_tab.size(); i++)
	{
		if (type_product_tab[i] == 1)
		{
			temp_index++;
			if (temp_index == 2)
			{
				return i;
			}
		}
	}
}

/**
 * A function that calculates the final price for an order at a pizzeria.
 *
 * @brief Calculation of the order price.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return float: order price.
 */
float calculate(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab)
{
	float price_sum = 0;
	int temp = 0;
	int index;
	static int temp_gratis_drink = 0;

	for (int i = 0; i < type_product_tab.size(); i++)
	{
		temp += type_product_tab[i];
	}

	for (int i = 0; i < price_product_tab.size(); i++)
	{
		price_sum += price_product_tab[i];
	}

	if (temp > 1)
	{
		index = calculate_promtion_fifty_fifty(price_product_tab, type_product_tab);
		price_sum = price_sum - (price_product_tab[index] / 2);

		if (temp_gratis_drink == 0)
		{
			name_product_tab.push_back("COCA-COLA - gratis");
			price_product_tab.push_back(0.00);
			type_product_tab.push_back(0);
		}
		temp_gratis_drink++;
	}

	if (price_sum >= 100.00)
	{
		price_sum *= 0.8;
	}
	
	return price_sum;
}

/**
 * A function that displays a basket with an order.
 *
 * @brief Show an order.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param string promotion_fifty_fifty: activation of the fifty fifty promotion.
 * @return float: order price.
 */
float show_order(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, string promotion_fifty_fifty)
{
	float price;

	price = calculate(name_product_tab, price_product_tab, type_product_tab);

	for (int i = 0; i < name_product_tab.size(); i++)
	{

		if (calculate_promtion_fifty_fifty(price_product_tab, type_product_tab) == i)
		{
			cout << i + 1 << ". " << name_product_tab[i] << "    " << showpoint << fixed << setprecision(2) << price_product_tab[i] / 2 << "z�" << promotion_fifty_fifty << endl;
		}
		else
		{
			cout << i + 1 << ". " << name_product_tab[i] << "    " << showpoint << fixed << setprecision(2) << price_product_tab[i] << "z�" << endl;
		}
	}

	if (price >= 100.00)
	{
		cout << "Rabat 20% zosta� naliczony przy zam�wieniu za minimum 100z�" << endl;
		cout << "Suma: " << showpoint << fixed << setprecision(2) << price << "z�" << endl;
	}
	else
	{
		cout << "Suma: " << showpoint << fixed << setprecision(2) << price << "z�" << endl;
	}
	
	return price;
}

/**
 * Function displaying the shopping cart.
 *
 * @brief Displays shopping cart.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void show_shopping_cart(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab)
{
	char select = '0';
	float price;

	cout << "Pizzeria AdMat" << endl << endl << "Tw�j koszyk:" << endl;
	price = show_order(name_product_tab, price_product_tab, type_product_tab, " - promocja 50/50");
	
	cout << endl << "Wybierz opcj�" << endl;
	cout << "1. Zap�a�" << endl;
	cout << "2. Wr��" << endl;
	cout << "Wybierz: ";

	while (select != '1' && select != '2')
	{
		cin >> select;
		while (getchar() != '\n');

		if (select != '1' && select != '2')
		{
			printf("Nieprawid�owy wyb�r!\n");
			printf("Wybierz ponownie opcj�: ");
		}
	}

	switch (select)
	{
	case '1':
		select = '0';

		system("cls");
		receipt_generate(name_product_tab, price_product_tab, type_product_tab, price);

		break;

	case '2':
		select = '0';

		system("cls");
		menu(name_product_tab, price_product_tab, type_product_tab);

		break;

	default:
		break;
	}
}
