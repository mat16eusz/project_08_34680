/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#ifndef DRINK_H_
#define DRINK_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * Function displaying the drinks available for sale in the restaurant.
 *
 * @brief Displays drink.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void drink(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab);

#endif
