/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#include "Receipt.h"
#include <iostream>
#include <vector>
#include <iomanip> 
#include <fstream>
#include "Shopping_cart.h"

using namespace std;

/**
 * The function saves the receipt for orders based on the basket.
 *
 * @brief Saving a receipt.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param string promotion_fifty_fifty: activation of the fifty fifty promotion.
 * @param float price: order price.
 * @return int: 1 file opened incorrectly, 0 success.
 */
int write_file(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, string promotion_fifty_fifty, float price)
{
    fstream file;

    file.open(file_name, ios::out);

    if (file.good() != true)
    {
        cout << "Nie mo�na otworzy� pliku" << endl;
        file.close();

        return 1;
    }

    file << "Pizzeria AdMat" << endl << endl << "PARAGON FISKALNY" << endl << endl;

    for (int i = 0; i < name_product_tab.size(); i++)
    {

        if (calculate_promtion_fifty_fifty(price_product_tab, type_product_tab) == i)
        {
            file << i + 1 << ". " << name_product_tab[i] << "    " << showpoint << fixed << setprecision(2) << price_product_tab[i] / 2 << "zl" << promotion_fifty_fifty << endl;
        }
        else
        {
            file << i + 1 << ". " << name_product_tab[i] << "    " << showpoint << fixed << setprecision(2) << price_product_tab[i] << "zl" << endl;
        }
    }

    if (price >= 100.00)
    {
        file << "Rabat 20% zostal naliczony przy zamowieniu za minimum 100zl" << endl;
        file << "Suma: " << showpoint << fixed << setprecision(2) << price << "zl" << endl;
    }
    else
    {
        file << "Suma: " << showpoint << fixed << setprecision(2) << price << "zl" << endl;
    }

    file.close();

    return 0;
}

/**
 * Function that generates a receipt for orders based on the basket.
 *
 * @brief Generating a receipt.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param float price: order price.
 */
void receipt_generate(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, float price)
{
    cout << "Utworzono paragon" << endl;
    write_file("Receipt.txt", name_product_tab, price_product_tab, type_product_tab, "", price);
}
