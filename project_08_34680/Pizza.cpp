/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#include "Pizza.h"
#include <iostream>
#include <vector>
#include <string>
#include "Read_file.h"
#include "Menu.h"
#include "Shopping_cart.h"

using namespace std;

/**
 * Function displaying the pizza available for sale in the restaurant.
 *
 * @brief Displays pizza.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void pizza(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab)
{
    string select;
    int select_int;

    cout << "Wybierz opcj�" << endl;
    read_file("Offer/Pizza.txt");
    cout << "16. Wr��" << endl;
    cout << "Wyb�r produktu: ";

    while (select != "1" && select != "2" && select != "3" && select != "4" && select != "5" && select != "6" && select != "7" && select != "8" && select != "9" && select != "10" && select != "11" && select != "12" && select != "13" && select != "14" && select != "15" && select != "16")
    {
        cin >> select;
        while (getchar() != '\n');

        if (select != "1" && select != "2" && select != "3" && select != "4" && select != "5" && select != "6" && select != "7" && select != "8" && select != "9" && select != "10" && select != "11" && select != "12" && select != "13" && select != "14" && select != "15" && select != "16")
        {
            printf("Nieprawid�owy wyb�r!\n");
            printf("Wybierz ponownie opcj�: ");
        }
    }
    select_int = stoi(select);

    switch (select_int)
    {
    case 1:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 0, type_product_tab);

        break;

    case 2:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 1, type_product_tab);

        break;

    case 3:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 2, type_product_tab);

        break;

    case 4:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 3, type_product_tab);

        break;

    case 5:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 4, type_product_tab);

        break;
    case 6:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 5, type_product_tab);

        break;
    case 7:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 6, type_product_tab);

        break;
    case 8:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 7, type_product_tab);

        break;
    case 9:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 8, type_product_tab);

        break;
    case 10:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 9, type_product_tab);

        break;
    case 11:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 10, type_product_tab);

        break;
    case 12:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 11, type_product_tab);

        break;
    case 13:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 12, type_product_tab);

        break;
    case 14:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 13, type_product_tab);

        break;
    case 15:
        select = "0";

        system("cls");
        add_product_to_shopping_cart("Offer/Pizza.txt", name_product_tab, price_product_tab, 14, type_product_tab);

        break;
    case 16:
        system("cls");
        menu(name_product_tab, price_product_tab, type_product_tab);
        break;

    default:
        break;
    }
}
