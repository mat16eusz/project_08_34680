/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#ifndef SHOPPING_CART_H_
#define SHOPPING_CART_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * The function adds the selected product to the basket.
 *
 * @brief Adding a product to the cart.
 * @param string file_name: filename to read the file.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param int indeks: index number to read the product.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void add_product_to_shopping_cart(string file_name, vector<string>& name_product_tab, vector<float>& price_product_tab, int indeks, vector<int>& type_product_tab);

/**
 * Function responsible for imposing the fifty fifty promotion.
 *
 * @brief Fifty Fifty Promotion Calculation.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return int: index of the second pizza.
 */
int calculate_promtion_fifty_fifty(vector<float>& price_product_tab, vector<int>& type_product_tab);

/**
 * A function that calculates the final price for an order at a pizzeria.
 *
 * @brief Calculation of the order price.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @return float: order price.
 */
float calculate(vector<float>& price_product_tab, vector<int>& type_product_tab);

/**
 * A function that displays a basket with an order.
 *
 * @brief Show an order.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 * @param string promotion_fifty_fifty: activation of the fifty fifty promotion.
 * @return float: order price.
 */
float show_order(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab, string promotion_fifty_fifty);

/**
 * Function displaying the shopping cart.
 *
 * @brief Displays shopping cart.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void show_shopping_cart(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab);

#endif
