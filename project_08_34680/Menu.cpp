/**
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 */

#include "Menu.h"
#include <iostream>
#include <vector>
#include "Pizza.h"
#include "Drink.h"
#include "Promotion.h"
#include "Shopping_cart.h"

using namespace std;

/**
 * A function that displays a menu that allows you to order products and make payments.
 *
 * @brief Displays menu.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void menu(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab)
{
    char select = '0';

    cout << "Pizzeria AdMat" << endl << endl;
    cout << "Oferta" << endl << endl;
    cout << "Wybierz opcj�" << endl;
    cout << "1. Pizza" << endl;
    cout << "2. Napoje" << endl;
    cout << "3. Promocje" << endl;
    cout << "4. Koszyk" << endl;
    cout << "5. Wyj�cie" << endl;
    cout << "Wybierz opcj�: ";

    while (select != '1' && select != '2' && select != '3' && select != '4' && select != '5')
    {
        cin >> select;
        while (getchar() != '\n');

        if (select != '1' && select != '2' && select != '3' && select != '4' && select != '5')
        {
            printf("Nieprawid�owy wyb�r!\n");
            printf("Wybierz ponownie opcj�: ");
        }
    }

    switch (select)
    {
    case '1':
        select = '0';
        
        system("cls");
        pizza(name_product_tab, price_product_tab, type_product_tab);

        break;

    case '2':
        select = '0';

        system("cls");
        drink(name_product_tab, price_product_tab, type_product_tab);

        break;

    case '3':
        select = '0';

        system("cls");
        promotion(name_product_tab, price_product_tab, type_product_tab);

        break;

    case '4':
        select = '0';

        system("cls");
        show_shopping_cart(name_product_tab, price_product_tab, type_product_tab);

        break;

    case '5':
        exit(0);
        break;

    default:
        break;
    }
}
