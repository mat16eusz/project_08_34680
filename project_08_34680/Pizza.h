/**
 * @author    Adrian Ciocho�
 * @author    34275@student.pwsztar.edu.pl
 * @author    Mateusz Jasiak
 * @author    mateusz.jasiak.dev@gmail.com
 */

#ifndef PIZZA_H_
#define PIZZA_H_

#include <iostream>
#include <vector>

using namespace std;

/**
 * Function displaying the pizza available for sale in the restaurant.
 *
 * @brief Displays pizza.
 * @param vector<string>& name_product_tab: vector which stores the names of the products.
 * @param vector<float>& price_product_tab: vector storing the price of the products.
 * @param vector<int>& type_product_tab: vector storing the type of products.
 */
void pizza(vector<string>& name_product_tab, vector<float>& price_product_tab, vector<int>& type_product_tab);

#endif
